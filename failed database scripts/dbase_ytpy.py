from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey, Sequence



engine = create_engine('dan', echo = True)
metadata = MetaData()

videos = Table('videos', metadata,
	Column('id', Integer, primary_key=True),
	Column('descriere', String),
	Column('channel', String),
	Column('vizualizari', Integer),
	Column('likes', Integer),
	Column('dislikes', Integer)
	)

metadata.create_all(engine)