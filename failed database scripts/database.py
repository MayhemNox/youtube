import sqlite3
import pandas as pd


connect = sqlite3.connect('ytdb.sql')
cursor = connect.cursor()
cursor.execute('CREATE TABLE IF NOT EXISTS ytdbase (id integer PRIMARY KEY, descriere text NOT NULL, channel text NOT NULL, vizionari integer NOT NULL, likes integer NOT NULL, dislikes integer NOT NULL, FOREIGN KEY (id) REFERENCES projects (id))')



### PRINT TABS ###
desc = cursor.execute("select * from ytdbase")
print(list(map(lambda x: x[0], desc.description)))

print(pd.read_sql_table('ytdb.sql', connect))


# cursor.execute("DROP TABLE ytdbase")
