import mysql.connector
from youtube import ytpy


class Database:
    def __init__(self):
        self.mydb = mysql.connector.connect(
            host="127.0.0.1",
            user="root",
            passwd="password",
            database="mydb"
        )
        self.mycursor = self.mydb.cursor()

    def creator(self):
        self.mycursor.execute("CREATE TABLE IF NOT EXISTS Tabel (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), description VARCHAR(255), date DATE);")
        self.mydb.commit()

    def insertor(self, name, description, date):
        self.mycursor.execute("INSERT INTO Tabel (name, description, date) VALUES ('%s', '%s', '%s')".format(name, description, date))
        self.mydb.commit()


Database = Database()
Database.insertor('Dan', 'Gigel', '2019-07-17')