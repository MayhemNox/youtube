#!/usr/bin/env python3
import os
import sqlite3


class Database:
    def __init__(self):
        db_directory = 'resources/data'
        if not os.path.exists(db_directory):
            os.makedirs(db_directory)
        db_file = db_directory+'/app.db'
        conn = sqlite3.connect(db_file)
        conn.row_factory = sqlite3.Row
        self.conn = conn

    def recreate_tables(self):
        connection = self.conn
        sqlCommands = ['''DROP TABLE IF EXISTS 'data_table';''', '''CREATE TABLE `data_table` (
    `id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, video_link TEXT, `uploader` TEXT,
    `title`	TEXT, `desc`	TEXT, `the_date`	TEXT, `views` INTEGER, `likes`	INTEGER, `dislikes` INTEGER, `video_comments` INTEGER);''']
        for command in sqlCommands:
            try:
                connection.execute(command)
                connection.commit()
            except sqlite3.OperationalError:
                print("Command skipped: ", command)

    def insert_table(self, video_link, uploader, title, desc, the_date, views, likes, dislikes, comments):
        conn = self.conn
        connection = conn.cursor()
        try:
            connection.execute("INSERT INTO data_table (video_link, uploader, title, desc, the_date, views, likes, dislikes, video_comments) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", (video_link, uploader, title, desc, the_date, views, likes, dislikes, comments))
            conn.commit()
            connection.close()
            return connection.lastrowid
        except sqlite3.OperationalError as error:
            connection.close()
            return error

    def get_info(self):

        conn = self.conn
        cur = conn.cursor()

        rows = [rows[0] for rows in cur.execute("SELECT video_link FROM data_table")]
        return rows


# create = Database().recreate_tables()
# print(create)