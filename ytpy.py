import os
import requests
import sys
import pprint
import google_auth_oauthlib.flow
import googleapiclient.discovery
import googleapiclient.errors
import json
from googleapiclient.discovery import build
import reset_db

DEVELOPER_KEY = "AIzaSyCuHehxn8VBi6HjdkrSyX6DpLn4c5dVE3Q"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"
base_url = 'https://www.googleapis.com/youtube/v3/videos?part=statistics&id='
base_comment_url = "https://www.googleapis.com/youtube/v3/commentThreads?key=%&textFormat=plainText&part=snippet&videoId=%"
url_dev_key = '&key=AIzaSyCuHehxn8VBi6HjdkrSyX6DpLn4c5dVE3Q'


def youtube_search(q, max_results=50, order="relevance", token=None, location=None, location_radius=None):
    youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION, developerKey=DEVELOPER_KEY)
    search_response = youtube.search().list(
        q=q,
        type="video",
        pageToken=token,
        order=order,
        part="id,snippet",
        maxResults=max_results,
        location=location,
        locationRadius=location_radius).execute()

    for response in search_response.get("items"):
        video_uploader = response['snippet']['channelTitle']
        video_title = response['snippet']['title']
        video_description = response['snippet']['description']
        video_date = (response['snippet']['publishedAt'][0:10] + ' Time: ' + response['snippet']['publishedAt'][11:19])
        video_id = response['id']['videoId']
        video_url = "https://www.youtube.com/watch?v=" + video_id

        if video_url not in reset_db.Database().get_info():
            try:
                req = requests.get(base_url + video_id + url_dev_key)
                req = req.json()
                # print(req)
                video_view_count = req['items'][0]['statistics']['viewCount']
                video_likes = req['items'][0]['statistics']['likeCount']
                video_dislikes = req['items'][0]['statistics']['dislikeCount']
                video_comments = req['items'][0]['statistics']['commentCount']

                reset_db.Database().insert_table(video_url, video_uploader, video_title, video_description, video_date, video_view_count, video_likes, video_dislikes, video_comments)

            except:
                print('ceva nu a mers')



# youtube_search(sys.argv[0])
youtube_search('UFC')

